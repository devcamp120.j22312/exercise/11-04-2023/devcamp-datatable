import { Container, Grid, Pagination } from "@mui/material"
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { useEffect, useState } from "react";

const DataTable = () => {
    const numberRow = 3; // Số dòng trong 1 trang
    const [rows, setRows] = useState([]);
    const [totalPage, setTotalPage] = useState(0);
    const [currentPage, setcurrentPage] = useState(1);

    const fetchAPI = async (url) => {
        const response = await fetch(url);

        const data = response.json();

        return data;
    }

    const handlerChangePage = (event, value) => {
        setcurrentPage(value);
    }

    useEffect(() => {
        fetchAPI("https://jsonplaceholder.typicode.com/users")
        .then((data) =>{
            setTotalPage(Math.ceil(data.length / numberRow));
            setRows(data.slice((currentPage - 1) * numberRow, currentPage * numberRow));
        })
        .catch((error) => {
            console.error(error.message);
        })
    }, [currentPage])

    return (
        <Container>
            <Grid container>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                    <TableContainer component={Paper}>
                        <Table sx={{ minWidth: 650 }} aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Id</TableCell>
                                    <TableCell align="right">Name</TableCell>
                                    <TableCell align="right">Username</TableCell>
                                    <TableCell align="right">Email</TableCell>
                                    <TableCell align="right">Phone</TableCell>
                                    <TableCell align="right">Website</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {rows.map((row) => (
                                    <TableRow
                                        key={row.id}
                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                    >
                                        <TableCell component="th" scope="row">
                                            {row.id}
                                        </TableCell>
                                        <TableCell align="right">{row.name}</TableCell>
                                        <TableCell align="right">{row.username}</TableCell>
                                        <TableCell align="right">{row.email}</TableCell>
                                        <TableCell align="right">{row.phone}</TableCell>
                                        <TableCell align="right">{row.website}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
            </Grid>
            <Grid container justifyContent={"end"}>
                <Grid item>
                <Pagination count={totalPage} defaultPage={currentPage} onChange={handlerChangePage} />
                </Grid>
            </Grid>
        </Container>
    )
}

export default DataTable;